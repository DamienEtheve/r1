properties([pipelineTriggers([GenericTrigger(causeString: 'Generic Cause', regexpFilterExpression: '', regexpFilterText: '', token: 'testwhebooks')])])

pipeline {
  agent {
    docker { image 'node:latest' }
  }
  stages {
    stage('init') {
      steps {
        sh 'npm --version'
        sh 'npm i'
      }
    }
    stage('run') {
      steps {
        sh 'npm start'
      }
    }
  }
  post {
    success {  
      mail bcc: '', body: "<b>Pipeline webhooks</b><br>Project: ${env.JOB_NAME} <br>Build Number: ${env.BUILD_NUMBER} <br> URL de build: ${env.BUILD_URL}", cc: '', charset: 'UTF-8', from: 'damien@syscea.com', mimeType: 'text/html', replyTo: '', subject: "SUCCESS CI: Pipeline -> ${env.JOB_NAME}", to: "damien.etheve@esiroi.re, choarau@syscea.com";  
    }  
    failure {  
      mail bcc: '', body: "<b>Pipeline webhooks</b><br>Project: ${env.JOB_NAME} <br>Build Number: ${env.BUILD_NUMBER} <br> URL de build: ${env.BUILD_URL}", cc: '', charset: 'UTF-8', from: 'damien@syscea.com', mimeType: 'text/html', replyTo: '', subject: "ERROR CI: Pipeline -> ${env.JOB_NAME}", to: "damien.etheve@esiroi.re, choarau@syscea.com";  
    }
  }
}
