import com.cloudbees.hudson.plugins.folder.AbstractFolder

def newParamsList = []

newParamsList.add(booleanParam(
  name: "reloadScriptParamOnly",
  defaultValue: false,
  description: "If checked will load script params and exit"
))

newParamsList.add(booleanParam(
  name: "removeDisableJobs",
  defaultValue: false,
  description: "If checked will remove disable jobs"
))

properties([
    disableConcurrentBuilds(),
    parameters(newParamsList)
])

/* ============    reload script params    ================= */

if( params.reloadScriptParamOnly ) {
   return
}

def lastList = null
def newList = null
def difference = null
 
pipeline {
  agent {
    docker {
      image 'node:latest'
    }
  }
  stages {
    stage('create destination folder') {
        steps {
            jobDsl scriptText: """
                folder('generated') {
                    displayName('generated')
                    description('Folder for generated jobs')
                }
            """    
        }
    }
    // TODO: if there is no file (state 0)
    stage("check if state 0") {
        steps {
            script {
                if(fileExists("${workspace}/array.json") == false) {
                    // run fetch
                    git credentialsId: 'xio_private_key', url: 'git@bitbucket.org:DamienEtheve/r1.git'
                } else {
                    echo "nothing to do"
                }
            }
        }
    }
    
    stage('get last file') {
      steps {
        script {
            lastList = readJSON file: "${workspace}/array.json"
        }
      }
    }

    stage('clone') {
      steps {
        deleteDir()
        git credentialsId: 'xio_private_key', url: 'git@bitbucket.org:DamienEtheve/r1.git'
      }
    }

    stage('get new file') {
      steps {
        script {
            newList = readJSON file: "${workspace}/array.json"
        }
      }
    }

    // differences entre les fichiers
    stage("diff files") {
        steps {
            script {
                echo "=========== last List =========="
                for (item in lastList) {
                    echo item
                }
                echo "=========== new List =========="
                for (item in newList) {
                    echo item
                }
                echo "=========== DIFF =============="
                def commons = newList.intersect(lastList)
                difference = newList.plus(lastList)
                difference.removeAll(commons)
                
                def commonDiffAndNew = difference.intersect(newList)
                difference.removeAll(commonDiffAndNew)
                
                for (item in difference) {
                    echo item
                }
            }
        }
    }
    
    stage('remove disable jobs') {
        steps {
            script {
                if(params.removeDisableJobs == true) {
                    // pour tout les items dans le dossier
                    Jenkins.instance.getItemByFullName("generated", AbstractFolder).getItems()
                        .findAll { it instanceof ParameterizedJobMixIn.ParameterizedJob || it instanceof AbstractFolder }
                        .each {
                            // it.makeDisabled(true)
                            println(it.disabled)
                            if (it.disabled == true) {
                                it.delete()
                                println("Deleted job: [$it.fullName]")
                            } else {
                                println("nothing to delete")
                            }
                        }
                                        
                } 
            }
        }
    }
    
    stage('disable a obselet jobs') {
        steps {
            script {
                if (!difference.isEmpty()) {
                    for (item in difference) {
                        def myItem = Jenkins.instance.getItemByFullName("generated/pipeline-${item}-scm");
                        if (myItem != null) {
                            // myItem.delete()
                            myItem.setDisabled(true)
                        } else { 
                            currentBuild.result = 'FAIL'
                            error('Name does not exist')
                        }
                    }
                }
            }
        }
    }

    stage('foreach') {
      steps {
        script {
          // parcour list 
          for (item in newList) {
            stage("Create pipeline ${item}") {
                // is job disabled ? 
                try {
                    def myItem = Jenkins.instance.getItemByFullName("generated/pipeline-${item}-scm");
                    myItem.setDisabled(false)
                    echo "job enabled"
                } catch(Exception e) {
                    echo "skip job enabled"
                }
                
                // create/update a pipeline job
                jobDsl scriptText: """
                    pipelineJob('generated/pipeline-${item}-scm') {
                        definition {
                            cpsScm {
                                scm {
                                    git{
                                        remote {
                                            url('git@bitbucket.org:DamienEtheve/r2.git')
                                            credentials('xio_private_key')
                                        }
                                    }
                                }
                                scriptPath('Jenkinsfiledynamic.groovy')
                            }
                        } 
                    }
                """
            }
          }
        }
      }
    }
  }
}
