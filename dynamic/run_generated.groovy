def newList = null

pipeline {
  agent {
    docker {
      image 'node:latest'
    }
  }
  stages {
    stage('clone') {
      steps {
        deleteDir()
        git credentialsId: 'xio_private_key', url: 'git@bitbucket.org:DamienEtheve/r1.git'
      }
    }

    stage('get new file') {
      steps {
        script {
            newList = readJSON file: "${workspace}/array.json"
        }
      }
    }
    
    stage('run all pipelines') {
      steps {
        script {
          // parcour list 
          for (item in newList) {
            sh "echo 'start  -  ${item}'"
            build job: "generated/pipeline-${item}-scm", wait: false
          }
        }
        
      }
    }
  }
}


