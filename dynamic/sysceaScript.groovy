@Library("syscea-common")_

/*  package.json 
    lstWorkspace.[client|server| ... ].[last|new]
*/

def foldersName = ["test", "build", "docker"]
def lstUtils = ["client", "server"]

def lstWorkspaces = [:]
lstWorkspaces.put("client",[:])
lstWorkspaces.client.put("last", null)
lstWorkspaces.client.put("newf", null)
lstWorkspaces.client.put("difference", null)

lstWorkspaces.put("server",[:])
lstWorkspaces.server.put("last", null)
lstWorkspaces.server.put("newf", null)
lstWorkspaces.server.put("difference", null)

pipeline {
  agent {
    docker {
        image "syscea/jenkins-nodejs:latest"
    }
  }
  stages {
    stage('create destination folders') {
        steps {
            script {
                for (name in foldersName) {
                    jobDsl scriptText: """
                        folder('generated-${name}') {
                            displayName('generated-${name}')
                            description('Folder for generated-${name}')
                        }
                    """
                }
            }
            
        }
    }
    
    // TODO: if there is no file (state 0)
    stage("check if state 0") {
        steps {
            sh "ls -la"
            script {
                if(fileExists("${workspace}/package.json") == false) {
                    // git credentialsId: 'choarau-syscea-bitbucket', url: 'git@bitbucket.org:sysceadev/syscea-workspaces.git' // to change
                    checkout_workspaces()
                } else {
                    echo "nothing to do"
                }
            }
        }
    }
    
    stage('get last file') {
      steps {
            script {
                for (lst in lstUtils) {
                    def x = readJSON file: "workspaces/syscea-${lst}/package.json"
                    println(x.syscea.entryPoints)
                    lstWorkspaces["${lst}"].last = x.syscea.entryPoints
                }
            }
      }
    }

    stage('clone') {
      steps {
            deleteDir()  
            // git credentialsId: 'choarau-syscea-bitbucket', url: 'git@bitbucket.org:sysceadev/syscea-workspaces.git'
            checkout_workspaces()
            du_sh()
      }
    }

    stage('get new file') {
        steps {
            sh "ls -la"
            sh "ls -la workspaces/syscea-server"
            sh "ls -la workspaces/syscea-client"
            script {
                for (lst in lstUtils) {
                    def x = readJSON file: "workspaces/syscea-${lst}/package.json"
                    println(x.syscea.entryPoints)
                    lstWorkspaces["${lst}"].newf = x.syscea.entryPoints
                }
            }
        }
    }
    

    // differences entre les fichiers
    stage("diff files") {
        steps {
            script {
                for (lst in lstUtils) {
                    echo "___________=========== ${lst} ==========__________" // client | server
                    def newList  = lstWorkspaces["${lst}"].newf
                    def lastList = lstWorkspaces["${lst}"].last
                    def difference = lstWorkspaces["${lst}"].difference
                    
                    echo "=========== last List =========="
                    for (item in lastList) {
                        echo item
                    }
                    echo "=========== new List =========="
                    for (item in newList) {
                        echo item
                    }
                    echo "=========== DIFF =============="
                    def newLit
                    def commons = newList.intersect(lastList)
                    difference = newList.plus(lastList)
                    difference.removeAll(commons)
                    
                    def commonDiffAndNew = difference.intersect(newList)
                    difference.removeAll(commonDiffAndNew)
                    
                    for (item in difference) {
                        echo item
                    }

                    lstWorkspaces["${lst}"].difference = difference
                }
           
            }
        }
    }

    
    stage('delete a obselet job list') {
        steps {
            script {
                
                    for (lst in lstUtils) {
                        def difference = lstWorkspaces["${lst}"].difference
                        if (!difference.isEmpty()) {
                            for (item in difference) {
                                for (name in foldersName) {
                                    def myItem = Jenkins.instance.getItemByFullName("generated-${name}/${item}");
                                    if (myItem != null) {
                                        myItem.delete()
                                    } else { 
                                        currentBuild.result = 'FAIL'
                                        error('Name does not exist')
                                    }
                                }
                                
                            }
                        }
                    }
                    
                
            }
        }
    }
    
    stage('foreach') {
      steps {
        script {
            for (lst in lstUtils) { // pour client | server | .... 
                for (item in lstWorkspaces["${lst}"].newf) { // pour tout les éléments de client | server | .... 
                    //stage("[Create] ${item}") {
                     //   steps {
                            // create a pipeline job
                    println("==========create ${item}=========")
                    for (name in foldersName) {
                        // stage("[GENERATE] - generated-${name}/${item}") {
                            jobDsl scriptText: """
                                pipelineJob('generated-${name}/${item}') {
                                    definition {
                                        cpsScm {
                                            scm {
                                                git{
                                                    remote {
                                                        url('git@bitbucket.org:DamienEtheve/r2.git')
                                                        credentials('xio_private_key')
                                                    }
                                                }
                                            }
                                            scriptPath('Jenkinsfiledynamic.groovy')
                                        }
                                    }
                                }
                            """.stripIndent()
                        //}
                        
                    }
                    
                }
            }
        }
      }
    }
  }
}
