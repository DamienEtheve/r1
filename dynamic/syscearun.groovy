@Library("syscea-common")_

def foldersName = ["test", "build", "docker"]
def lstUtils = ["client", "server"]
def targetList = []

pipeline {
  agent {
    docker {
      image "syscea/jenkins-nodejs:latest"
    }
  }
  stages {
    stage('clone') {
      steps {
        deleteDir()
        checkout_workspaces()
      }
    }

    stage('get new file') {
      steps {
        script {
            for (lst in lstUtils) {
                def x = readJSON file: "workspaces/syscea-${lst}/package.json"
                // println(x.syscea.entryPoints)
                targetList.add(x.syscea.entryPoints)
            }
            targetList = targetList.flatten()
        }
      }
    }
    
    stage('run all pipelines') {
      steps {
        script {
          // parcour list 
            println(targetList)
            for (item in targetList) {
               for (name in foldersName) {
                    sh "echo 'run generated-${name} / ${item}'"
                    build job: "generated-${name}/${item}", wait: false
               }
           }
        }
      }
    }
  }
}


