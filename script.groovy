pipeline {
  agent {
    docker { 
      image 'node:latest'
      customWorkspace "/var/jenkins_home/workspace/generated/${env.JOB_NAME}"
    }
  }
  stages {
    stage('init') {
      steps {
        sh 'npm --version'
        sh 'npm i'
      }
    }
    stage('run') {
      steps {
        sh "echo  'hello world'"
      }
    }
  }
}