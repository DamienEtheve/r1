import  { Bitbucket } from 'bitbucket';

const clientOptions = {
  baseUrl: 'https://api.bitbucket.org/2.0',
  auth: {
    username: 'EtheveDamien',
    password: 'Ne!mad07a9117d31B!tbucket'
  }
}

const bitbucket = new Bitbucket(clientOptions);

async function getIssue (number : string) {
  console.log(`--- get issue number ${number}`);
  try {
    return await bitbucket.issue_tracker.get({ 
        issue_id: number,
        repo_slug: "r1",
        workspace:"DamienEtheve"
      })
  } catch (err) {
    throw err;
  }
}

async function getIssues () {
  console.log(`--- get issues ---`);
  try {
      return await bitbucket.issue_tracker.list({ 
        repo_slug: "r1",
        workspace:"DamienEtheve"
      })
  } catch (err) {
    throw err;
  }
}

// usage
getIssues()
  .then((val) => {
    const { data, headers }  = val;
    console.log(data);
  })
  .catch((err) => {
    throw err;
  })
  