@Library("syscea-common")_

/* ==================  global variables  ================== */

def foldersName = ["test", "build", "docker"]
def lstUtils    = ["client", "server"]
def targetList  = [] // contain all app ["client-app-sale", "server-app-appointment", etc...]

def entryPointChoices = [
	"",
	"client",
	"server",
	"client-app-sale",
	"client-app-project",
	"client-app-appointment",
	"server-app-appointment"
]

/* ============== define pipeline properties ============== */

def newParamsList = []

newParamsList.add(booleanParam(
  name: "reloadScriptParamOnly",
  defaultValue: false,
  description: "If checked will load script params and exit"
))

newParamsList.add(booleanParam(
  name: "cleanup",
  defaultValue: false,
  description: "If checked reset workspace directory"
))

newParamsList.add(booleanParam(
  name: "skipInstall",
  defaultValue: false,
  description: "If checked will skip yarn install"
))

newParamsList.add(booleanParam(
  name: "silentMode",
  defaultValue: false,
  description: "If checked do not send emails"
))

newParamsList.add(booleanParam(
  name: "forceBuild",
  defaultValue: false,
  description: "If unchecked do not process unchanged packages [--forceBuild]"
))

newParamsList.add(booleanParam(
  name: "dockerBuild",
  defaultValue: false,
  description: "If checked build docker image [--dockerBuild]"
))

newParamsList.add(choice(
    name: 'entryPoint',
    description: 'Select an entry point or none for all [--entryPoint]', 
    choices: entryPointChoices.join("\n")
))

newParamsList.add(choice(
  name: 'targetEnvironment',
  description: 'Select target env [--env]',
  choices: ["development", "development:prod", "staging", "production"].join("\n")
))

properties([
    disableConcurrentBuilds(),
    parameters(newParamsList)
])

/* ============    reload script params    ================= */

if( params.reloadScriptParamOnly ) {
   return
}

/* ====================      Pipeline   ==================== */

pipeline {
  agent {
    docker {
      image "syscea/jenkins-nodejs:latest"
    }
  }
  stages {
    stage("clean") {
      steps {
        script {
          if (params.cleanup) {
            deleteDir()
            echo "Full cleanup - deleteDir"
          } else {
            echo "Skip cleanup"
          }
        }
      }
    }
    
    stage('clone') {
      steps {
        checkout_workspaces()
      }
    }
    
    stage("install packages") {
        steps {
        script {
          if (params.cleanup || !params.skipInstall) {
            sh "chmod +x tools/scripts/installAll.sh"
            sh "npm run installAll:linux"
          } else {
            echo "Skip install"
          }
        }
      }
    }

    stage('get new file') {
      steps {
        script {
          for (lst in lstUtils) {
              def x = readJSON file: "workspaces/syscea-${lst}/package.json"
              targetList.add(x.syscea.entryPoints)
          }
          targetList = targetList.flatten()
        }
      }
    }

    stage('== generate test - build - docker ==') {
      steps {
        script {
          for (fName in foldersName) {
            stage("generated ${fName}") {
              if (params.entryPoint == "") {
                  echo "all"

                  for (item in targetList) {
                    build job: "generated-${fName}/${item}", parameters: [
                      booleanParam(name: 'silentMode', value: true),
                      booleanParam(name: 'forceBuild', value: params.forceBuild),
                      stringParam(name: 'entryPoint', value: "${item}"),
                      stringParam(name: 'targetEnvironment', value: params.targetEnvironment),
                    ], wait : true
                  }
              } else if (params.entryPoint == "client") {
                  echo "all clients"

                  for (item in targetList) {
                    def values = item.split('-')
                    if (values[0] == "client") {
                      build job: "generated-${fName}/${item}", parameters: [
                        booleanParam(name: 'silentMode', value: true),
                        booleanParam(name: 'forceBuild', value: params.forceBuild),
                        stringParam(name: 'entryPoint', value: "${item}"),
                        stringParam(name: 'targetEnvironment', value: params.targetEnvironment),
                      ], wait : true
                    }
                  }
                  
              } else if (params.entryPoint == "server") {
                  echo "all server"

                  for (item in targetList) {
                    def values = item.split('-')

                    if (values[0] == "server") {
                      build job: "generated-${fName}/${item}", parameters: [
                        booleanParam(name: 'silentMode', value: true),
                        booleanParam(name: 'forceBuild', value: params.forceBuild),
                        stringParam(name: 'entryPoint', value: "${item}"),
                        stringParam(name: 'targetEnvironment', value: params.targetEnvironment),
                      ], wait : true
                    }
                  }
              } else  {
                echo "specific target"

                build job: "generated-${fName}/${params.entryPoint}", parameters: [
                  booleanParam(name: 'silentMode', value: true),
                  booleanParam(name: 'forceBuild', value: params.forceBuild),
                  stringParam(name: 'entryPoint', value: params.entryPoint),
                  stringParam(name: 'targetEnvironment', value: params.targetEnvironment),
                ], wait : true
              }
            }
          }
        }
      }
    }
  }

  post {
    always {
      echo "End - ${env.JOB_NAME}"
		  env_info()
    }
    success {
      script {
        if (!params.silentMode) {
          mail from: "no-reply@syscea.com", to: "damien.etheve@esiroi.re", replyTo: "",
            cc: "",	bcc: "",
            charset: "UTF-8", mimeType: "text/html",
            subject: "SUCCESS CI: Pipeline -> ${env.JOB_NAME}",
            body: "<b>Pipeline</b><br>Project: ${env.JOB_NAME} <br>Build Number: ${env.BUILD_NUMBER} <br> URL de build: ${env.BUILD_URL}";
        }
      }
    }
    failure {
      script {
        if (!params.silentMode) {
          mail from: "no-reply@syscea.com", to: "damien.etheve@esiroi.re", replyTo: "",
            cc: "",	bcc: "",
            charset: "UTF-8", mimeType: "text/html",
            subject: "ERROR CI: Pipeline -> ${env.JOB_NAME}",
            body: "<b>Pipeline</b><br>Project: ${env.JOB_NAME} <br>Build Number: ${env.BUILD_NUMBER} <br> URL de build: ${env.BUILD_URL}";
        }
      }
    }
  }
}
